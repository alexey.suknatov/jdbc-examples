import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class TestSqlite {

	@Test
	public void testSqlite() {
		try (Connection connection = connectDb()) {
			createTable(connection);

			insert(connection, 1, "Ivanov", 521);
			insert(connection, 2, "Petrov", 531);
			insert(connection, 3, "Sidorov", 511);
			selectAll(connection);

			update(connection, 3, 521);
			selectAll(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection connectDb() throws SQLException {
		// path to the SQLite database file can be either relative or absolute

		// To connect to an in-memory database, you use the following connection string:
		// jdbc:sqlite::memory:
		String url = "jdbc:sqlite:sqlite/students.db";

		// create a connection to the database
		// Next, the DriverManager class gets a database connection based on the connection string
        Connection conn = DriverManager.getConnection(url);
		System.out.println("Connection to SQLite has been established.");
		return conn;
	}

	public void createTable(Connection connection) throws SQLException {
		String sql = "CREATE TABLE IF NOT EXISTS students (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	\"group\" integer NOT NULL\n"
                + ");";

		try (Statement stmt = connection.createStatement()) {
		 	// create a new table
			stmt.execute(sql);
		}
	}

	public void insert(Connection conn, int id, String name, int group) throws SQLException {
		String sql = "INSERT INTO students(id,name, \"group\") VALUES(?,?,?)";

		try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setInt(1, id);
			pstmt.setString(2, name);
			pstmt.setInt(3, group);
            pstmt.executeUpdate();
        }
    }

    /**
     * Update data of a students specified by the id
     */
    public void update(Connection conn, int id, int group) throws SQLException {
        String sql = "UPDATE students SET "
                + "\"group\" = ? "
                + "WHERE id = ?";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            // set the corresponding param
            pstmt.setInt(1, group);
            pstmt.setInt(2, id);

            // update
            pstmt.executeUpdate();
        }
    }

    /**
     * select all rows in the students table
     */
    public void selectAll(Connection connection) throws SQLException {
        String sql = "SELECT id, name, \"group\" FROM students";

        try (Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

			System.out.println("id\tname\tgroup");

			// loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") +  "\t" +
                                   rs.getString("name") + "\t" +
                                   rs.getInt("group"));
            }
        }
    }
}
